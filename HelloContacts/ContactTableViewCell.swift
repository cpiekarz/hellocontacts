//
//  ContactTableViewCell.swift
//  HelloContacts
//
//  Created by Cody Piekarz on 12/19/18.
//  Copyright © 2018 Cody Piekarz. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var contactImage: UIImageView!
    override func prepareForReuse() {
        super.prepareForReuse()
        contactImage.image = nil
    }
}
